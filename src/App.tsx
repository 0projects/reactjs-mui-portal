import {useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import {
  Container,
  CircularProgress,
  Box,
  ThemeProvider,
  createTheme,
  CssBaseline,
} from "@mui/material";
import { useSelector } from "react-redux";
import { AppBar } from "./components/app-bar";
import { Drawer } from "./components/drawer";
import { Dialog } from "./components/dialog";
import { Snackbar } from "./components/snackbar";
import "./App.css";
import { loadAppSchema } from "./redux/slices/app";
import { show as showDialog } from "./redux/slices/dialog";
import { check as checkUser } from "./redux/slices/user";
import { useAppDispatch } from "./hooks";
import { Router } from "./routes";

function App() {
  const { app, user } = useSelector((state: any) => state);
  const dispatch = useAppDispatch();

  //const ioClient = useRef();
  // let [ioClient, setIOClient] = useState({});

  useEffect(() => {
    // console.log(`app.loadingStatus -> '%s'`, app.loadingStatus, app);
    switch (app.loadingStatus) {
      case "none": {
        dispatch(loadAppSchema());
        break;
      }
      case "done": {
        document.title = `${app.name} |`;
        if (!user.isLoggedin) {
          // console.log(`app.schema.loginDialogId`, app.schema.loginDialogId);
          if (app.schema.loginDialogId) {
            dispatch(showDialog({ diaglogId: app.schema.loginDialogId }));
          }
        }
        break;
      }
      default:
        break;
    }
  }, [app.loadingStatus]);

  useEffect(() => {
    switch (user.checkingStatus) {
      case "none": {
        dispatch(checkUser());
        break;
      }
      case "done": {
        if (user.isLoggedin) {
          dispatch(loadAppSchema());
        }
        break;
      }
    }
  }, [user.checkingStatus]);

  const theme = createTheme(app.schema.theme || {});
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <Container className="app-container" maxWidth={false}>
          <Box
            className="overlay"
            sx={{ display: app.loadingStatus === "loading" ? "block" : "none" }}
          >
            <CircularProgress className="loading-image" />
          </Box>
          <Snackbar />
          <AppBar />
          <Drawer />
          <Dialog />
          <Router navigationItems={app.navigationItems} />
        </Container>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
