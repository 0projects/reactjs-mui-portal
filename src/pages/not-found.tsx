import Box from "@mui/material/Box";
import { Typography } from "@mui/material";

export function NotFound() {

  return (
    <Box className="not-found-box" >
        <Typography variant="h3">
          Không tìm thấy trang
        </Typography>
        <Typography>
          Trang bạn đang truy cập không tồn tại.
        </Typography>
    </Box>
  );
}
