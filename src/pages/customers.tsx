import React, { useState, useEffect } from "react";
import {
  Typography,
  Breadcrumbs,
  LinearProgress,
} from "@mui/material";
import { useSelector } from "react-redux";
import { useLocation, Link } from "react-router-dom";
import { useAppDispatch } from "../hooks";
import { loadSchema } from "../redux/slices/page";

const PageSchema = {
  title: 'Quản lý khách hàng',
  description: 'Quản lý thông tin các khách hàng hiện diện trên hệ thống',
}

export function Customers() {
  const { app, user } = useSelector((state: any) => state);
  const dispatch = useAppDispatch();
  const location = useLocation();

  let [isProcessing, setIsProcessing] = useState(false);

  return (
    <React.Fragment>
      <Breadcrumbs aria-label="breadcrumb">
        <Link color="inherit" to="/">
          Trang Chủ
        </Link>
        <Typography color="text.primary">
          {PageSchema.title}
        </Typography>
      </Breadcrumbs>
      <Typography variant="h1" className="page-title">
        {PageSchema.title}
      </Typography>
      <Typography variant="subtitle1">
        {PageSchema.description}
      </Typography>
      <LinearProgress
        sx={{ visibility: isProcessing ? "visible" : "hidden" }}
      />
    </React.Fragment>
  );
}
