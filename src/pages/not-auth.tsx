import Box from "@mui/material/Box";
import { useSelector } from "react-redux";
import { Button, Typography } from "@mui/material";
import { useAppDispatch } from "../hooks";
import { show as showDialog } from "../redux/slices/dialog";

export function NotAuth() {
  const dispatch = useAppDispatch();
  const { app } = useSelector((state: any) => state);
  return (
    <Box className="not-found-box">
      <Typography variant="h3">Bạn chưa đăng nhập</Typography>
      <Typography>Vui lòng ấn vào nút dưới đây để đăng nhập.</Typography>
      <br/>
      <Button
        size="large"
        variant='contained'
        onClick={() => dispatch(showDialog({ diaglogId: app.schema.loginDialogId }))}
      >
        Đăng nhập
      </Button>
    </Box>
  );
}
