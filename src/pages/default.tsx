import React, { useState, useEffect } from "react";
import {
  Typography,
  Breadcrumbs,
  Paper,
  IconButton,
  LinearProgress,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Divider,
  FormGroup,
  FormControlLabel,
  FormLabel,
  Checkbox,
  Grid,
  Chip,
  FormControl,
  InputLabel,
  FormHelperText,
  Slider,
  Select,
  MenuItem,
  OutlinedInput,
  ListItemText,
  Link as LinkComp,
  TablePagination,
  TableFooter,
} from "@mui/material";
import { useSelector } from "react-redux";
import { useLocation, Link } from "react-router-dom";
import { useAppDispatch } from "../hooks";
import { loadSchema } from "../redux/slices/page";

const PageSchema = {
  title: 'Tiêu đề trang',
  description: 'Mô tả trang',
}

export function Default() {
  const { app, user } = useSelector((state: any) => state);
  const dispatch = useAppDispatch();
  const location = useLocation();

  let [isProcessing, setIsProcessing] = useState(false);

  return (
    <React.Fragment>
      <Breadcrumbs aria-label="breadcrumb">
        <Link color="inherit" to="/">
          Trang Chủ
        </Link>
        <Typography color="text.primary">
          {PageSchema.title}
        </Typography>
      </Breadcrumbs>
      <Typography variant="h1" className="page-title">
        {PageSchema.title}
      </Typography>
      <Typography variant="subtitle1">
        {PageSchema.description}
      </Typography>
      <LinearProgress
        sx={{ visibility: isProcessing ? "visible" : "hidden" }}
      />
    </React.Fragment>
  );
}
