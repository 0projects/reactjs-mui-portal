import React from "react";
import { useSelector } from "react-redux";
import Container from "@mui/material/Container";
import { Routes, Route } from "react-router-dom";
import { Default } from "../pages/default";
import { NotFound } from "../pages/not-found";
import { NotAuth } from "../pages/not-auth";
import { Customers } from "../pages/customers";

export function Router(props: any) {
  const navigationItems = props.navigationItems || [];
  const user = useSelector((state: any) => state.user);

  return (
    <Container className={"router-container"} maxWidth={false}>
      <Routes>
        <Route path="/" element={<Default />} />
        {navigationItems.map((item: any, i: number) => {
          let page = <NotFound />;
          if (
            item.requireAuthentication === true &&
            user.isLoggedin === false
          ) {
            page = <NotAuth />;
          } else {
            switch (item.pageTemplate) {
              case "customers": {
                page = <Customers />;
                break;
              }
              default: {
                page = <NotFound />;
                break;
              }
            }
          }
          return <Route key={i} path={item.path} element={page} />;
        })}
        <Route path="/*" element={<NotFound />} />
      </Routes>
    </Container>
  );
}
