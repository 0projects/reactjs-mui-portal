import { createSlice, createAsyncThunk, current } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import { show as showSnackbar } from "./snackbar";
import { setObjectPropertyByPath } from "../../helpers/object-helper";

export interface DataState {
  stage: "none" | "processing" | "done" | "failed";
}

const initialState: DataState = {
  stage: "none",
};

export const dataSlice = createSlice({
  name: "data",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setData: (
      state: any,
      action: { type: string; payload: { name: string; value: any } }
    ) => {  
      setObjectPropertyByPath(
        state,
        action.payload.name,
        action.payload.value
      );
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadData.pending, (state) => {
        state.stage = "processing";
      })
      .addCase(loadData.fulfilled, (state: any, action) => {
        state[action.payload.name] = action.payload.value;
        state.stage = "done";
      })
      .addCase(loadData.rejected, (state) => {
        state.stage = "failed";
      });
    builder
      .addCase(submitData.pending, (state) => {
        state.stage = "processing";
      })
      .addCase(submitData.fulfilled, (state: any, action) => {
        setObjectPropertyByPath(
          state,
          action.payload.name,
          action.payload.value
        );
        state.stage = "done";
      })
      .addCase(submitData.rejected, (state) => {
        state.stage = "failed";
      });
    builder
      .addCase(requestFromAPI.pending, (state) => {
        state.stage = "processing";
      })
      .addCase(requestFromAPI.fulfilled, (state: any, action) => {
        setObjectPropertyByPath(
          state,
          action.payload.name,
          action.payload.value
        );
        state.stage = "done";
      })
      .addCase(requestFromAPI.rejected, (state) => {
        state.stage = "failed";
      });
  },
});

// Actions

export const { setData } = dataSlice.actions;

// ----------------------------------------------------------------------

export const loadData = createAsyncThunk(
  "data/load",
  async (
    config: {
      method: "get";
      url: string;
      headers: object;
      dataName: string;
    },
    { dispatch }
  ) => {
    const axiosConfig = {
      method: config.method,
      url: config.url,
      headers: config.headers,
    };
    const result = await axios(axiosConfig as any)
      .then((res) => res.data)
      .catch((error) => error);
    if (result instanceof AxiosError) {
      const statusCode = result.response ? result.response.status : "";
      switch (statusCode) {
        case 401: {
          dispatch(
            showSnackbar({
              title: "Bạn không có quyền tải dữ liệu này!",
              type: "error",
            })
          );
          break;
        }
        case 404:
        case 400: {
          dispatch(
            showSnackbar({
              title: "Yêu cầu dữ liệu không hợp lệ!",
              type: "error",
            })
          );
          break;
        }
        default: {
          dispatch(
            showSnackbar({
              title: "Tải dữ liệu không thành công!",
              type: "error",
            })
          );
          break;
        }
      }
      throw result;
    } else {
      dispatch(
        showSnackbar({ title: "Tải dữ liệu thành công!", type: "success" })
      );
      return { name: config.dataName, value: result };
    }
  }
);

export const submitData = createAsyncThunk(
  "data/submit",
  async (
    config: {
      method: "post" | "put" | "patch" | "delete";
      url: string;
      headers: object;
      data: any;
      dataPathToStoreResult: string;
    },
    { dispatch }
  ) => {
    const axiosConfig = {
      method: config.method,
      url: config.url,
      headers: config.headers,
      data: config.data,
    };

    console.log(axiosConfig);
    const result = await axios(axiosConfig as any)
      .then((res) => res.data)
      .catch((error) => error);
    if (result instanceof AxiosError) {
      console.log(result);
      let code = result.response?.data.statusCode;
      let message = result.response?.data.message;
      switch (code) {
        case 401: {
          dispatch(
            showSnackbar({
              title: "Bạn không có quyền thực hiện hành động này!",
              content: message,
              type: "error",
            })
          );
          break;
        }
        case 404:
        case 400: {
          dispatch(
            showSnackbar({
              title: "Dữ liệu không hợp lệ!",
              content: message,
              type: "error",
            })
          );
          break;
        }
        default: {
          dispatch(
            showSnackbar({
              title: "Lỗi!",
              content: message,
              type: "error",
            })
          );
          break;
        }
      }
      throw result;
    } else {
      dispatch(
        showSnackbar({ message: "Thao tác thành công!", type: "success" })
      );
      return { name: config.dataPathToStoreResult, value: result };
    }
  }
);

export const requestFromAPI = createAsyncThunk(
  "data/request-from-api",
  async (
    config: {
      method: "get" | "post" | "put" | "patch" | "delete";
      url: string;
      headers: object;
      data: any;
      extractDataFrom: string;
      storeDataTo: string;
    } = {
      method: "get",
      url: "/",
      headers: {},
      data: {},
      extractDataFrom: '',
      storeDataTo: 'response',
    },
    { dispatch }
  ) => {
    const axiosConfig = {
      method: config.method,
      url: config.url,
      headers: config.headers,
      data: config.data,
    };

    console.log(axiosConfig);
    const result = await axios(axiosConfig as any)
      .then((res) => config.extractDataFrom ? setObjectPropertyByPath(res.data,config.extractDataFrom, undefined): res.data)
      .catch((error) => error);
    if (result instanceof AxiosError) {
      let code = result.response?.data.statusCode;
      let message = result.response?.data.message;
      switch (code) {
        case 401: {
          dispatch(
            showSnackbar({
              title: "Bạn không có quyền thực hiện hành động này!",
              content: message,
              type: "error",
            })
          );
          break;
        }
        case 404:
        case 400: {
          dispatch(
            showSnackbar({
              title: "Dữ liệu không hợp lệ!",
              content: message,
              type: "error",
            })
          );
          break;
        }
        default: {
          dispatch(
            showSnackbar({
              title: "Lỗi!",
              content: message,
              type: "error",
            })
          );
          break;
        }
      }
      throw result;
    } else {
      dispatch(
        showSnackbar({ message: "Thao tác thành công!", type: "success" })
      );

      return { name: config.storeDataTo, value: result };
    }
  }
);

export default dataSlice.reducer;
