import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
export interface SnackbarState {
  type: 'info' | 'warning' | 'success' | 'error';
  title: string;
  content: string;
  isShown: boolean;
}

const initialState: SnackbarState = {
  type: 'info',
  title: '',
  content: '',
  isShown: false,
};

export const snackbarSlice = createSlice({
  name: "snackbar",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    toggle: (state: SnackbarState) => {
      state.isShown = !state.isShown;
    },
    show: (state: SnackbarState, action: any) => {
      state.type = action.payload.type;
      state.title = action.payload.title || action.payload.message;
      state.content = action.payload.content || '';
      state.isShown = true;
    },
  },
});

// Actions

export const { toggle, show } = snackbarSlice.actions;

export default snackbarSlice.reducer;
