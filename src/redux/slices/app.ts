import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { readCookie } from "../../helpers/cookie-helper";
import { stringToObject } from "../../helpers/object-helper";
export interface AppState {
  id: string;
  name: string;
  schema: object;
  navigationItems: object[];
  isDrawerShown: boolean;
  loadingStatus: "none" | "loading" | "failed" | "done";
}

const initialState: AppState = {
  id: "",
  name: "",
  schema: {},
  navigationItems: [],
  isDrawerShown: false,
  loadingStatus: "none",
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    toggleDrawer: (state: AppState) => {
      state.isDrawerShown = !state.isDrawerShown;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadAppSchema.pending, (state) => {
        state.loadingStatus = "loading";
      })
      .addCase(loadAppSchema.fulfilled, (state, action) => {
        state.loadingStatus = "done";
        state.id = action.payload.id;
        state.name = action.payload.name;
        state.navigationItems = action.payload.navigationItems;
        state.schema = action.payload.schema;
      })
      .addCase(loadAppSchema.rejected, (state) => {
        state.loadingStatus = "failed";
      });
  },
});

// Actions

export const { toggleDrawer } = appSlice.actions;

// ----------------------------------------------------------------------

export const loadAppSchema = createAsyncThunk("app/loadAppSchema", async () => {
  const appSchema = {
    id: "123456",
    name: "Portal",
    schema: {
      icon: "app",
      title: "Portal",
      description: "",
      loginDialogId: "629c648b3d87b937b03901ae",
      changePasswordDialogId: "632741d5c4b5ba4c4f58bfb4",
      theme: {
        palette: {
          primary: {
            main: "#1f3b9a",
            light: "#2548c2",
            dark: "#172d77",
            contrastText: "#fff",
          },
        },
      },
    },
    navigationItems: [
      {
        id: "629ecfc5f43d0b62de96c13f",
        order: 5,
        parentId: "",
        name: "Group function",
        icon: "folder_shared",
        path: "/",
        pageId: "62883c90a0fd00b4e7f1fa96",
        pageTemplate: "",
        requireAuthentication: true,
      },
      {
        id: "62a429e6f43d0b62deab494a",
        order: 0,
        parentId: "629ecfc5f43d0b62de96c13f",
        name: "Function",
        icon: "contact",
        path: "/customers",
        pageId: "62a42a20f43d0b62deab4d64",
        pageTemplate: "customers",
        requireAuthentication: true,
      },
    ],
  };
  return appSchema;
});

export default appSlice.reducer;
