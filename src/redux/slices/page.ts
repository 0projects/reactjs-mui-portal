import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import { setData } from "./data";
export interface PageState {
  id: string;
  name: string;
  description: string;
  data: any;
  schema: {
    title: "";
    description: "";
    actions: [];
    components: [];
    buttons: [];
  };
  loadingStatus: "none" | "loading" | "failed" | "done";
}

const initialState: PageState = {
  id: "",
  name: "",
  description: "",
  data: {},
  schema: {
    title: "",
    description: "",
    actions: [],
    components: [],
    buttons: [],
  },
  loadingStatus: "none",
};

export const pageSlice = createSlice({
  name: "page",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    logout: (state: PageState) => {},
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadSchema.pending, (state) => {
        state.loadingStatus = "loading";
      })
      .addCase(loadSchema.fulfilled, (state, action) => {
        state.loadingStatus = "done";
        state.id = action.payload.id;
        state.name = action.payload.name;
        state.description = action.payload.description;
        state.schema = action.payload.schema;
      })
      .addCase(loadSchema.rejected, (state) => {
        state.loadingStatus = "failed";
      });
  },
});

// Actions

export const { logout } = pageSlice.actions;

// ----------------------------------------------------------------------

export const loadSchema = createAsyncThunk(
  "page/loadSchema",
  async (pageId: string, { dispatch }) => {
    if (pageId) {
      const apiURL = `${process.env.REACT_APP_UI_API}/pages/${pageId}/schema`;
      const response = await axios.get(apiURL);

      const axiosConfig = {
        method: "get",
        url: `${process.env.REACT_APP_UI_API}/pages/${pageId}/schema`,
        headers: {},
      };
      const result = await axios(axiosConfig as any)
        .then((res) => res.data)
        .catch((error) => error);
      if (result instanceof AxiosError) {
        const statusCode = result.response ? result.response.status : "";
        switch (statusCode) {
          case 401: {
            console.warn(`Không có quyền tải Page Schema pageId='${pageId}'`);
            break;
          }
          case 404:
          case 400: {
            console.warn(`Lỗi tải Page Schema pageId='${pageId}'`);
            break;
          }
          default: {
            console.warn(`Lỗi tải Page Schema pageId='${pageId}'`);
            break;
          }
        }
        throw result;
      } else {
        console.info(
          `%cTải thành công Page Schema pageId='${pageId}'`,
          "color: green"
        );

        if (response.data.schema) {
          // set filters of CRUD page
          if (Array.isArray(response.data.schema.filters)) {
            let pageFilter: any = { pageIdex: 0, pageSize: 10 };
            for (let i = 0; i < response.data.schema.filters.length; i++) {
              const filter: any = response.data.schema.filters[i];
              if (filter.name) pageFilter[filter.name] = "";
            }
            dispatch(setData({ name: "filters", value: pageFilter }));
          }
        }
        return response.data;
      }
    } else {
      throw new Error("pageId is required!");
    }
  }
);

export default pageSlice.reducer;
