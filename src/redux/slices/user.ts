import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  bakeCookie,
  readCookie,
  deleteCookie,
} from "../../helpers/cookie-helper";
import { show as showSnackbar } from "../slices/snackbar";
import { toggle as toggleDialog } from "../slices/dialog";
import { stringToObject } from "../../helpers/object-helper";

export interface UserState {
  id: string;
  orgId: string;
  username: string;
  fullname: string;
  roles: string[];
  accessToken: string;
  refreshToken: string;
  isLoggedin: boolean;
  expiresAt: number;
  checkingStatus: "none" | "processing" | "done" | "failed";
}

const initialState: UserState = {
  id: "",
  orgId: "",
  username: "",
  fullname: "",
  accessToken: "",
  refreshToken: "",
  isLoggedin: false,
  checkingStatus: "none",
  expiresAt: 0,
  roles: [],
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    check: (state: UserState) => {
      const cookieValue = readCookie("user");
      const user = stringToObject(cookieValue);
      if (user) {
        if (user.expiresAt > new Date().getTime()) {
          state.accessToken = user.accessToken;
          state.checkingStatus = "done";
          state.expiresAt = user.expiresAt;
          state.fullname = user.fullname;
          state.id = user.id;
          state.isLoggedin = true;
          state.refreshToken = user.refreshToken;
          state.username = user.username;
          state.orgId = user.orgId;
          state.roles = user.roles || [];
        } else {
          state = { ...initialState };
        }
      } else {
        state = { ...initialState };
      }
    },
    logout: (state: UserState) => {
      deleteCookie("user");
      state.isLoggedin = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state) => {
        state.checkingStatus = "none";
      })
      .addCase(login.fulfilled, (state, action) => {
        const now = new Date();
        state.id = action.payload.user.id;
        state.orgId = action.payload.user.orgId;
        state.username = action.payload.user.username;
        state.fullname = action.payload.user.fullname;
        state.accessToken = action.payload.accessToken;
        state.refreshToken = action.payload.refreshToken;
        state.expiresAt = new Date(
          now.getTime() + action.payload.expiresIn * 1000
        ).getTime();
        state.isLoggedin = true;
        state.roles = action.payload.user.roles;
        state.checkingStatus = "done";

        bakeCookie("user", state);
      })
      .addCase(login.rejected, (state) => {
        state.isLoggedin = false;
        state.checkingStatus = "failed";
      });
  },
});

// Actions

export const { check, logout } = userSlice.actions;

// ----------------------------------------------------------------------

export const login = createAsyncThunk(
  "user/login",
  async (loginObject: { username: string; password: string }, { dispatch }) => {
    const loginResult = {
      user: {
        id: "62a681ccf43d0b62dec8919e",
        username: "test@abc.com",
        fullname: "",
        orgId: "632434849aad3a5b1ba600b7",
        roles: [""],
      },
      accessToken:
        "123456",
      refreshToken:
        "AXPBj2DPKjZVOSCH9Q8R8c31WeD88IvI6euhxjFfz0sg6Jrf0DOJ1oI2bOQD2sZB",
      expiresIn: 35999,
    };
    dispatch(
      showSnackbar({ message: "Đăng nhập thành công!", type: "success" })
    );
    dispatch(toggleDialog());
    return loginResult;
  }
);

export default userSlice.reducer;
