import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
export interface DialogState {
  id: string;
  name: string;
  description: string;
  data: any;
  schema: {
    title: string;
    description: string;
    actions: object[];
    components: object[];
    buttons: object[];
  };
  isShown: boolean;
  loadingStatus: "none" | "loading" | "failed" | "done";
}

const initialState: DialogState = {
  id: "",
  name: "",
  description: "",
  data: {},
  schema: {
    title: "",
    description: "",
    actions: [],
    components: [],
    buttons: [],
  },
  isShown: false,
  loadingStatus: "none",
};

export const dialogSlice = createSlice({
  name: "dialog",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    toggle: (state: DialogState) => {
      state.isShown = !state.isShown;
    },
    showD: (state: DialogState, action: any) => {
      if (action.payload.diaglogId) {
        state.id = action.payload.diaglogId;
        state.isShown = true;
        state.loadingStatus = "none";
      }
    },
    setData: (state: DialogState, action: any) => {
      state.data[action.payload.name] = action.payload.value;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(show.pending, (state) => {
        state.loadingStatus = "loading";
        state.isShown = true;
      })
      .addCase(show.fulfilled, (state, action) => {
        state.loadingStatus = "done";
        state.id = action.payload.id;
        state.name = action.payload.name;
        state.schema = action.payload.schema;
        state.schema.components.forEach((c: any) => {
          state.data[c.name] = "";
        });
      })
      .addCase(show.rejected, (state) => {
        state.loadingStatus = "failed";
      });
  },
});

// Actions

export const { toggle, setData, showD } = dialogSlice.actions;

// ----------------------------------------------------------------------

export const show = createAsyncThunk(
  "dialog/showDialog",
  async (dialogObj: { diaglogId: string }) => {
    const dialogResult = {
      id: "629c648b3d87b937b03901ae",
      name: "Đăng nhập",
      schema: {
        title: "Đăng nhập",
        description: "Vui lòng nhập thông tin email và mật khẩu để đăng nhập",
        components: [
          { type: "email-input", title: "Email", name: "username" },
          { type: "password-input", title: "Mật khẩu", name: "password" },
        ],
        buttons: [
          { title: "Đăng nhập", variant: "contained", action: "login" },
        ],
        actions: [
          {
            name: "login",
            path: "user.login",
            parameters: [
              { name: "username", value: "{{data.username}}" },
              { name: "password", value: "{{data.password}}" },
            ],
          },
          { name: "close", path: "dialog.close", parameters: [] },
        ],
      },
    };
    return dialogResult;
  }
);

export default dialogSlice.reducer;
