import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import appReducer from './slices/app';
import dialogReducer, { toggle as toggleDialog, show as showDialog } from './slices/dialog';
import userReducer, { login } from './slices/user';
import pageReducer from './slices/page';
import snackbarReducer, { toggle, show as showSnackbar } from './slices/snackbar';

export const store = configureStore({
  reducer: {
    app: appReducer,
    dialog: dialogReducer,
    user: userReducer,
    page: pageReducer,
    snackbar: snackbarReducer,
  },
});

export const rootActionCreators = {
  app: {

  },
  page: {

  },
  dialog: {
    toggle: toggleDialog,
    show: showDialog,
  },
  snackbar:{
    toggle,
    show: showSnackbar,
  },
  user: {
    login,
  }
}

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
