export function bakeCookie(name: string, value: object) {
  const cookieValue = typeof value === 'object' ? JSON.stringify(value) : value;
  const cookie = [
    name,
    "=",
    cookieValue,
    "; path=/;",
  ].join("");
  document.cookie = cookie;
}

export function readCookie(name: string) {
  var result = document.cookie.match(new RegExp(name + "=([^;]+)"));
  if(result){
    if(result.length === 2){
      return result[1];
    }
  }
  return '';
}

export function deleteCookie(name: string) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  
}
