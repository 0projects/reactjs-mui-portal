export function toDatetimeString(date: Date | string) {
    let formatedDateString = '';
    if(typeof date === 'string'){
        date = new Date(date);
    }
    formatedDateString = date.toLocaleString().split(',')[0]
    return formatedDateString;
}

export function toThousandSeperatorNumber(input: number | string) {
    if(input){
        return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }else{
        return '';
    }
}