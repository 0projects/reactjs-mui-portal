export function findByPathOfObject(
  object: object,
  path: string,
  defaultValue: any
) {
  return path.split(".").reduce((o: any, p: string) => {
    return o ? o[p] : defaultValue;
  }, object);
}

export function setObjectPropertyByPath(object: any, path: string, value: any) {
  const parts = path.split(".");
  const limit = parts.length - 1;
  for (let i = 0; i < limit; ++i) {
    const key = parts[i];
    object = object[key] ?? (object[key] = {});
  }
  const key = parts[limit];
  object[key] = value;
}

export function handleStringValueWithPathOfObject(
  object: object,
  value: string,
  defaultValue: any
) {
  let pathValueRegex = /{{[a-zA-Z0-9_.]+}}/g;
  let matchedPaths = value.match(pathValueRegex);
  let handledValue = "" + value;
  if (matchedPaths) {
    matchedPaths.forEach((matchedPath) => {
      let path = matchedPath.replace("{{", "").replace("}}", "");
      handledValue = handledValue.replace(
        matchedPath,
        findByPathOfObject(object, path, matchedPath).toString()
      );
    });
  }
  return handledValue;
}

export function stringToObject(stringToParse: string) {
  try {
    return JSON.parse(stringToParse);
  } catch (error) {
    return undefined;
  }
}

export function getTypeOf(v:any) {
  let type: any = typeof v;

  if(type === 'object') {
    if(Array.isArray(v)){
      type = 'array';
    }
    if (typeof v.getMonth === "function") {
      type = "datetime";
    }
  }

  return type;
}