export function convertDatetimeToStringFormat(value: string | number | Date, format: string) {
  if (!value) {
    return "";
  }
  let datetime = typeof value === 'object' ? value : new Date(value);
  let year = datetime.getFullYear();
  let month = datetime.getMonth() + 1;
  let day = datetime.getDate();
  let hour = datetime.getHours();
  let minute = datetime.getMinutes();
  let second = datetime.getSeconds();

  let monthStr = month.toString();
  let dayStr = day.toString();
  let hourStr = hour.toString();
  let minuteStr = minute.toString();
  let secondStr = second.toString();

  let result = "";

  if (month < 10) {
    monthStr = "0" + month;
  }
  if (day < 10) {
    dayStr = "0" + day;
  }
  if (hour < 10) {
    hourStr = "0" + hour;
  }
  if (minute < 10) {
    minuteStr = "0" + minute;
  }
  if (second < 10) {
    secondStr = "0" + second;
  }
  switch (format) {
    default:
    case "YYYY-MM-DD": {
      result = `${year}-${monthStr}-${dayStr}`;
      break;
    }
    case "DD-MM-YYYY": {
      result = `${dayStr}-${monthStr}-${year}`;
      break;
    }
    case "YYYY-MM-DD HH:mm:ss": {
      result = `${year}-${monthStr}-${dayStr} ${hourStr}:${minuteStr}:${secondStr}`;
      break;
    }
    case "HH:mm:ss": {
      result = `${hourStr}:${minuteStr}:${secondStr}`;
      break;
    }
  }
  return result;
}
export function toTimestamp(value: string | number, format = "") {
  switch (format) {
    default:
    case "YYYY-MM-dd HH:mm:ss": {
      var datetime = new Date(value);
      return datetime.getTime();
      break;
    }
  }
}

export function dateToTimestamp(strDate: string) {
  var datum = Date.parse(strDate);
  return datum / 1000;
}
export function secondToHHMMSS(value: string | number) {
  let hms = "00:00:00";
  if (value) {
    let totalSeconds = 0;
    let seconds = 0;
    let hours = 0;
    let minutes = 0;

    if (typeof value === "string") {
      totalSeconds = parseInt(value, 10);
    } else {
      totalSeconds = value;
    }

    hours = Math.floor(totalSeconds / 3600);
    minutes = Math.floor((totalSeconds - hours * 3600) / 60);
    seconds = totalSeconds - hours * 3600 - minutes * 60;

    if (totalSeconds > 0) {
      hms = "";
      if (hours < 10) {
        hms = "0" + hours;
      } else {
        hms = hours.toString();
      }
      if (minutes < 10) {
        hms += ":0" + minutes;
      } else {
        hms += ":" + minutes.toString();
      }
      if (seconds < 10) {
        hms += ":0" + seconds;
      } else {
        hms += ":" + seconds.toString();
      }
    }
  }
  return hms;
}

export function countDownDuration(date: Date) {
  let ms = 0;
  let now = new Date().getTime();
  if (date) {
    if (typeof date.getTime === "function") {
      ms = now - date.getTime();
    } else {
      ms = now - new Date(date).getTime();
    }
  }
  return Math.ceil(ms / 1000);
}
