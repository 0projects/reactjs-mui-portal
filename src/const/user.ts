export const roles = {
    uniOwner: 'uni.owner',
    orgOwner: 'org.owner',
    orgAdmin: 'org.admin',
    orgAgent: 'org.agent',
}