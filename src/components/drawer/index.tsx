import React, { ReactElement, ReactNode, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  Drawer as Drawer_,
  Box,
  Button,
  List,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemButton,
  Collapse,
  Typography,
  ListItemText,
} from "@mui/material";
import Icon from '@mui/material/Icon';
import ExpandLess from '@mui/icons-material/ExpandLess';
import { useAppSelector, useAppDispatch } from "../../hooks";
import { toggleDrawer } from "../../redux/slices/app";

export function Drawer() {
  const [expendedNavItems, setExpendedNavItems] = useState([]);
  const app = useSelector((state: any) => state.app);
  const dispatch = useAppDispatch();
  const menuItems = [];
  if (app) {
    if (app.navigationItems) {
      const parentNavItems = app.navigationItems.filter(
        (item: any) => item.parentId === ""
      ).sort((p: any, n: any) => (p.order - n.order));
      for (let i = 0; i < parentNavItems.length; i++) {
        const navItem = parentNavItems[i];
        const menuItem = {
          id: navItem.id,
          icon: navItem.icon,
          title: navItem.name,
          path: navItem.path,
          subItems: app.navigationItems
            .filter((item: any) => item.parentId === navItem.id.toString())
            .map((subItem: any) => {
              return {
                icon: subItem.icon,
                title: subItem.name,
                path: subItem.path,
              };
            }),
        };
        menuItems.push(menuItem);
      }
    }
  }
  return (
    <React.Fragment>
      <Drawer_
        anchor={"left"}
        open={app.isDrawerShown}
        onClose={() => dispatch(toggleDrawer())}
      >
        <Box
          role="presentation"
          onClick={() => dispatch(toggleDrawer())}
          onKeyDown={() => dispatch(toggleDrawer())}
        >
          <Box className="drawer-header">
            
            <Typography variant="h4" color="primary">
            {app.schema.icon?<Icon>{app.schema.icon}</Icon>:<React.Fragment/>} {app.schema.title}
            </Typography>
          </Box>
          <Divider />
          <List component="nav">
            {menuItems
              .sort((a: any, b: any) => (a.order > b.order ? 0 : 1))
              .map((item: any, i: number) => {
                const comp = (
                  <React.Fragment key={i}>
                    <ListItem disablePadding>
                      <ListItemButton href={item.path}>
                        {
                          item.icon ? 
                          <ListItemIcon>
                            <Icon color='primary'>{item.icon}</Icon>
                          </ListItemIcon>:
                          <React.Fragment />
                        }
                        <Link className={"link"} to={item.path}>
                          <ListItemText>
                            <Typography sx={{fontWeight:'bold'}} color="primary">{item.title}</Typography>
                          </ListItemText>
                        </Link>
                        <ExpandLess />
                      </ListItemButton>
                    </ListItem>
                    {item.subItems.length > 0 ? (
                      <Collapse in={true} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                          {item.subItems
                            .sort((a: any, b: any) =>
                              a.order > b.order ? 0 : 1
                            )
                            .map((subItem: any, j: number) => (
                              <ListItemButton
                                key={j}
                                sx={{ pl: 4 }}
                                href={subItem.path}
                              >
                                <Link className={"link"} to={subItem.path}>
                                  <ListItemText primary={subItem.title} />
                                </Link>
                              </ListItemButton>
                            ))}
                        </List>
                      </Collapse>
                    ) : (
                      <React.Fragment/>
                    )}
                  </React.Fragment>
                );
                return comp;
              })}
          </List>
        </Box>
      </Drawer_>
    </React.Fragment>
  );
}
