import { useSelector } from "react-redux";
import { Snackbar as Snackbar_, Alert, Slide, Typography, AlertTitle } from "@mui/material";

import { useAppDispatch } from "../../hooks";
import { toggle } from "../../redux/slices/snackbar";

export function Snackbar() {
  const snackbar = useSelector((state: any) => state.snackbar);
  const dispatch = useAppDispatch();
  return (
    <Snackbar_
      open={snackbar.isShown}
      autoHideDuration={4000}
      transitionDuration={500}
      TransitionComponent={Slide}
      onClose={() => {
        dispatch(toggle());
      }}
    >
      <Alert
        onClose={() => {
          dispatch(toggle());
        }}
        severity={snackbar.type}
        sx={{ width: "100%" }}
      >
        <AlertTitle>{snackbar.title}</AlertTitle>
        {snackbar.content}
      </Alert>
    </Snackbar_>
  );
}
