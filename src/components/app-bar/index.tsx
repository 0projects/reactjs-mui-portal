import axios from "axios";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import {
  AppBar as AppBar_,
  Menu,
  MenuItem,
  Box,
  Toolbar,
  Button,
  IconButton,
} from "@mui/material";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MenuIcon from "@mui/icons-material/Menu";

import { useAppDispatch } from "../../hooks";
import { toggleDrawer } from "../../redux/slices/app";
import { show as showDialog } from "../../redux/slices/dialog";
import { logout } from "../../redux/slices/user";

export function AppBar() {
  const dispatch = useAppDispatch();
  const { app, user } = useSelector((state: any) => state);
  const [anchorElUser, setAnchorElUser] = useState(null);

  const handleOpenUserMenu = (event: any) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };


  return (
    <React.Fragment>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar_ position="static">
          <Toolbar>
            <Box sx={{ flexGrow: 1 }}>
              <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                onClick={() => dispatch(toggleDrawer())}
              >
                <MenuIcon />
              </IconButton>
            </Box>

            {user.isLoggedin ? (
              <Box sx={{ flexGrow: 0 }}>
                <Button
                  variant="text"
                  color="inherit"
                  onClick={handleOpenUserMenu}
                  startIcon={<AccountCircle />}
                >
                  {user.username}
                </Button>
                <Menu
                  id="user-menu"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  <MenuItem
                    onClick={() =>
                      dispatch(
                        showDialog({
                          diaglogId: app.schema.changePasswordDialogId,
                        })
                      )
                    }
                  >
                    Đổi mật khẩu
                  </MenuItem>
                  <MenuItem onClick={() => dispatch(logout())}>
                    Đăng xuất
                  </MenuItem>
                </Menu>
              </Box>
            ) : (
              <Box sx={{ flexGrow: 0 }}>
                <Button
                  size="large"
                  color="inherit"
                  onClick={() =>
                    dispatch(
                      showDialog({ diaglogId: app.schema.loginDialogId })
                    )
                  }
                >
                  Đăng nhập
                </Button>
              </Box>
            )}
          </Toolbar>
        </AppBar_>
      </Box>
    </React.Fragment>
  );
}
