import {
  Table as Table_,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  TableFooter,
  Typography,
  Button,
} from "@mui/material";
import DoneIcon from "@mui/icons-material/Done";
import React from "react";
import { convertDatetimeToStringFormat } from "../../helpers/datetime-helper";

export default function Table(props: any) {
  const {
    schema,
    data,
    paging,
    onRowAction,
    onPageChange,
    onRowsPerPageChange,
  } = props;

  return (
    <TableContainer>
      <Table_>
        <TableHead>
          <TableRow>
            {schema.columns.map((c: any, i: number) => {
              return (
                <TableCell key={i} align={c.align}>
                  <Typography sx={{ fontWeight: "bold" }} color="primary">
                    {c.title}
                  </Typography>
                </TableCell>
              );
            })}
            {schema.rowActions.length > 0 ? (
              <TableCell align="right">
                <Typography
                  sx={{ fontWeight: "bold" }}
                  color="primary"
                ></Typography>
              </TableCell>
            ) : (
              <React.Fragment />
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((d: any, i: number) => {
            return (
              <TableRow key={i}>
                {schema.columns.map((c: any, j: number) => {
                  const value = d[c.name];
                  let cell;
                  let formatedValue =
                    value != undefined ? value.toString() : "";

                  if (Array.isArray(value)) {
                    formatedValue = value.join(", ");
                  }

                  switch (c.format) {
                    case "date": {
                      formatedValue = convertDatetimeToStringFormat(value, 'YYYY-MM-DD');
                      cell = (
                        <TableCell key={j}>
                          <Typography>{formatedValue}</Typography>
                        </TableCell>
                      );  
                      break;
                    }
                    case "datetime": {
                      formatedValue = convertDatetimeToStringFormat(value, 'YYYY-MM-DD HH:mm:ss');
                      cell = (
                        <TableCell key={j}>
                          <Typography>{formatedValue}</Typography>
                        </TableCell>
                      );
                      break;
                    }
                    case "number": {
                      break;
                    }
                    case "checkmark": {
                      cell = (
                        <TableCell key={j}>
                          <DoneIcon
                            color={
                              formatedValue === "yes" ||
                              formatedValue === "true"
                                ? "primary"
                                : "disabled"
                            }
                          />
                        </TableCell>
                      );
                      break;
                    }
                    case "text":
                    default: {
                      cell = (
                        <TableCell key={j}>
                          <Typography>{formatedValue}</Typography>
                        </TableCell>
                      );
                      break;
                    }
                  }
                  return cell;
                })}
                {schema.rowActions.length > 0 ? (
                  <TableCell align="right">
                    {schema.rowActions.map((a: any, i: number) => {
                      return (
                        <Button
                          key={i}
                          variant="text"
                          name={a.name}
                          onClick={() => {
                            onRowAction({ action: a, data: d });
                          }}
                        >
                          {a.title}
                        </Button>
                      );
                    })}
                  </TableCell>
                ) : (
                  <React.Fragment />
                )}
              </TableRow>
            );
          })}
        </TableBody>
        <TableFooter>
          <TablePagination
            count={paging.totalItems}
            page={paging.pageIndex}
            rowsPerPage={paging.pageSize}
            onPageChange={onPageChange}
            onRowsPerPageChange={onRowsPerPageChange}
          />
        </TableFooter>
      </Table_>
    </TableContainer>
  );
}
