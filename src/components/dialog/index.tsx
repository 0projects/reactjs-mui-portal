import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  FormControl,
  FormControlLabel,
  Checkbox,
  Button,
  TextField,
  Dialog as Dialog_,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  LinearProgress,
  Skeleton,
} from "@mui/material";

import { useAppDispatch } from "../../hooks";
import { toggle, show } from "../../redux/slices/dialog";
import { setData } from "../../redux/slices/data";
import { rootActionCreators } from "../../redux/store";
import {
  findByPathOfObject,
  getTypeOf,
  handleStringValueWithPathOfObject,
} from "../../helpers/object-helper";
import { convertDatetimeToStringFormat } from "../../helpers/datetime-helper";

export function Dialog() {
  const state = useSelector((state: any) => state);
  const dialog = state.dialog;
  const dispatch = useAppDispatch();
  const handleButtonClick = (e: any) => {
    const actionName = e.target.name;
    const action = dialog.schema.actions.find((a: any) => {
      return a.name === actionName;
    });

    if (action) {
      if (action.path === "dialog.close") {
        dispatch(toggle());
      } else {
        const functionToCall = findByPathOfObject(
          rootActionCreators,
          action.path,
          undefined
        );
        if (typeof functionToCall === "function") {
          try {
            const functionParams: any = {};
            action.parameters.forEach((p: any) => {
              if (typeof p.value === "string" || typeof p.value === "object") {
                const paramValueInString: string =
                  typeof p.value === "object"
                    ? JSON.stringify(p.value)
                    : p.value.toString();
                if (
                  paramValueInString.toString().indexOf("{{") === 0 &&
                  paramValueInString.toString().indexOf("}}") ===
                    paramValueInString.toString().length - 2
                ) {
                  const valuePath = paramValueInString
                    .replace("{{", "")
                    .replace("}}", "");
                  functionParams[p.name] = findByPathOfObject(
                    state,
                    valuePath,
                    undefined
                  );
                } else if (
                  paramValueInString.toString().indexOf("{{") > 0 &&
                  paramValueInString.toString().indexOf("}}") > 2
                ) {
                  let newValue = (functionParams[p.name] =
                    handleStringValueWithPathOfObject(
                      state,
                      paramValueInString,
                      ""
                    ));
                  if (typeof p.value === "object") {
                    functionParams[p.name] = JSON.parse(newValue);
                  } else {
                    functionParams[p.name] = newValue;
                  }
                } else {
                  functionParams[p.name] = p.value;
                }
              } else {
                functionParams[p.name] = p.value;
              }
            });
            dispatch(functionToCall(functionParams));
          } catch (error) {
            console.log(error);
          }
        }
      }
    }
  };

  useEffect(() => {
    // console.log(`dialog.loadingStatus -> '%s'`, dialog.loadingStatus, dialog);
    switch (dialog.loadingStatus) {
      case "none": {
        // Nothing todo here.
        break;
      }
      case "done": {
        /* dialog.schema.onStart.forEach((actionName: string) => {
          const action = dialog.schema.actions.find(
            (a: any) => a.name === actionName
          );
          if (action) {
            const paramObject: any = {};
            action.parameters.forEach((p: any) => {
              paramObject[p.name] = p.value;
            });
            dispatch(loadData(paramObject));
          }
        }); */
        break;
      }
      default:
        break;
    }
  }, [dialog.loadingStatus]);

  return (
    <Dialog_
      open={dialog.isShown}
      onClose={() => dispatch(toggle())}
      fullWidth
      fullScreen={dialog.schema.isFullscreen || false}
    >
      {dialog.loadingStatus === "done" ? (
        <DialogTitle color="primary">{dialog.schema.title}</DialogTitle>
      ) : (
        <Skeleton variant="text" />
      )}

      <LinearProgress
        sx={{
          visibility: dialog.loadingStatus === "loading" ? "visible" : "hidden",
        }}
      />
      <DialogContent>
        {dialog.loadingStatus === "done" ? (
          <React.Fragment>
            <DialogContentText>{dialog.schema.description}</DialogContentText>
            {dialog.schema.components.map((comp: any, index: number) => {
              let returnComp = <React.Fragment />;
              let compValue = findByPathOfObject(
                state.data,
                comp.name,
                undefined
              );
              let textfieldType = comp.type.replace("-input", "");
              let valueDataType: string = getTypeOf(compValue);

              if (comp.dataType) {
                valueDataType = comp.dataType;
              } else {
                valueDataType = "string";
                if(compValue === undefined){
                  compValue = '';
                }
              }

              switch (valueDataType) {
                case "object": {
                  if (comp.type === "textarea-input") {
                    compValue = JSON.stringify(compValue, null, "\t");
                  }
                  break;
                }
                case "array-of-objects": {
                  if (comp.type === "textarea-input") {
                    compValue = JSON.stringify(compValue, null, "\t");
                  }
                  break;
                }
                case "string": {
                  if (comp.type === "date-input") {
                    compValue = convertDatetimeToStringFormat(
                      compValue,
                      "YYYY-MM-DD"
                    );
                  }
                  if (comp.type === "datetime-input") {
                    compValue = convertDatetimeToStringFormat(
                      compValue,
                      "YYYY-MM-DD HH:mm:ss"
                    );
                  }
                  break;
                }
                case "phone":
                case "number": {
                  textfieldType = "number";
                  break;
                }
                default: {
                  break;
                }
              }

              switch (comp.type) {
                case "checkbox-input": {
                  returnComp = (
                    <FormControl key={index} fullWidth variant="standard">
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={compValue}
                            name={comp.name}
                            onChange={(e) => {
                              if (comp.disabled !== true) {
                                dispatch(
                                  setData({
                                    name: comp.name,
                                    value: e.target.checked,
                                  })
                                );
                              }
                            }}
                          />
                        }
                        label={comp.title}
                      />
                    </FormControl>
                  );
                  break;
                }
                case "text-input":
                case "textarea-input":
                case "email-input":
                case "password-input":
                case "number-input":
                case "phone-input": {
                  returnComp = (
                    <FormControl key={index} fullWidth variant="standard">
                      <TextField
                        autoFocus
                        margin="dense"
                        id={comp.name}
                        label={comp.title}
                        name={comp.name}
                        type={textfieldType}
                        multiline={
                          comp.type === "textarea-input" ? true : false
                        }
                        rows={comp.rows || 6}
                        fullWidth
                        variant="standard"
                        value={compValue}
                        disabled={comp.disabled === true}
                        onChange={(e) => {
                          if (comp.disabled !== true) {
                            let newValue: any = e.target.value;
                            switch (valueDataType as string) {
                              case "array": {
                                newValue = e.target.value.split(",");
                                break;
                              }
                              case "array-of-strings": {
                                newValue = e.target.value.split(",");
                                break;
                              }
                              case "array-of-objects": {
                                newValue = JSON.parse(e.target.value);
                                break;
                              }
                              case "number": {
                                newValue = parseInt(e.target.value);
                                break;
                              }
                              case "object": {
                                try {
                                  newValue = JSON.parse(e.target.value);
                                } catch (error) {
                                  return;
                                }

                                break;
                              }
                              default:
                                break;
                            }
                            dispatch(
                              setData({ name: comp.name, value: newValue })
                            );
                          }
                        }}
                      />
                    </FormControl>
                  );
                  break;
                }
                case "date-input":
                case "datetime-input": {
                  returnComp = (
                    <FormControl key={index} fullWidth variant="standard">
                      <TextField
                        key={index}
                        autoFocus
                        margin="dense"
                        id={comp.name}
                        label={comp.title}
                        name={comp.name}
                        type={comp.type === "date-input" ? "date" : "datetime"}
                        fullWidth
                        variant="standard"
                        value={compValue}
                        disabled={comp.disabled === true}
                        onChange={(e) => {
                          if (comp.disabled !== true) {
                            let newValue: any = e.target.value;
                            dispatch(
                              setData({ name: comp.name, value: newValue })
                            );
                          }
                        }}
                      />
                    </FormControl>
                  );
                  break;
                }
                default:
                  break;
              }
              return returnComp;
            })}
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Skeleton variant="text" />
            <Skeleton variant="rectangular" height={300} />
          </React.Fragment>
        )}
      </DialogContent>
      <DialogActions>
        {dialog.schema.buttons.map((button: any, index: number) => {
          return (
            <Button
              key={index}
              variant={button.variant}
              name={button.action}
              onClick={handleButtonClick}
            >
              {button.title}
            </Button>
          );
        })}
      </DialogActions>
    </Dialog_>
  );
}
